#include<bits/stdc++.h>
using namespace std;
int MAX=1e6+1;
long long MOD=1e9+7;
int primes[1000005]={1};
void prime_sieve(int *primes){
	bool prime[MAX];
    memset(prime, true, sizeof(prime));
 
    for (int p = 2; p * p <= MAX; p++)
    { 

        if (prime[p] == true)
        {
           
            for (int i = p * p; i <= MAX; i += p)
                prime[i] = false;
        }
    }
 
    // Print all prime numbers
    for (int p = 2; p <= MAX; p++){
        if (prime[p])
            primes[p]=p;
    }
}
 int countDivisors(int n)
{

	int divisors=1;
    for(int i=2;primes[i]<=sqrt(n);i++)
    {
    	if(n%primes[i]==0){
    		int count=0;
    		while(n%primes[i]==0)
    		{
    			count++;
    			n/=primes[i];
    		}
    		divisors*=(count+1);

    	}
    	

    }
    if(n!=1) {
    	divisors*=2;
    } 


    return divisors;
}
long long divisorSum(long long n)
{
	long long sum=0;
	 for (int i = 1; i <= sqrt(n); i++) {
        if (n % i == 0) {
 
            // check if divisors are equal
           
               // printf("%d ", i);
            	sum=sum%MOD;
                 sum+=n/i;
                 sum+=i;
                // push the second divisor in the vector
                //v.push_back(n / i);
            
        }
    }
    return sum%MOD;
}
int main() {
	
 //prime_sieve(primes);
 
 	long long x;
 	cin>>x;
 //cout<<countDivisors(x)<<endl;
 cout<<divisorSum(x);

	return 0;
}
