#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pb push_back
#define boost ios_base::sync_with_stdio(false);cin.tie(NULL);
void solve()
{
   ll n;
   cin>>n;
   vector<ll> vec;
   vec.pb(n);
   while(n!=1)
   {
   	if(n%2==0)
   	{
      n=n/2;
      vec.pb(n);
   	}
   	else
   	{
   		n=n*3+1;
   		vec.pb(n);
   	}
   }
   for(auto x:vec)
   {
   	cout<<x<<" ";
   }
}
int main()
{
    ll t=1;
    //cin>>t;
    while(t--)
        solve();
}