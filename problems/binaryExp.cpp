#include<bits/stdc++.h>
using namespace std;

//#include <ext/pb_ds/tree_policy.hpp>
#ifndef ONLINE_JUDGE
#define debug(x) cout<<#x<<" :: "<<x<<endl;
#define debug2(x,y) cout<<#x<<" :: "<<x<<"\t"<<#y<<" :: "<<y<<endl;
#define debug3(x,y,z) cout<<#x<<" :: "<<x<<"\t"<<#y<<" :: "<<y<<"\t"<<#z<<" :: "<<z<<endl;
#define debug4(x,y,z,w) cout<<#x<<" :: "<<x<<"\t"<<#y<<" :: "<<y<<"\t"<<#z<<" :: "<<z<<"\t"<<#w<<" :: "<<w<<endl;
#else
#define debug(...)
#define debug2(...)
#define debug3(...)
#define debug4(...)
#endif
typedef long long ll;
typedef long long ll1;
typedef long double ld;
typedef vector<int> vi;
typedef pair<int,int> pi;
const ll MAX=1e6+5;
const double pii=atan(1)*4;
ll MOD=1e9+7;
#define pb push_back
#define se second
#define fi first
#define sb __builtin_popcount
#define maxe max_element
#define mine min_element
//using namespace __gnu_pbds;
#define os1 tree<ll, null_type,less<ll>, rb_tree_tag,tree_order_statistics_node_update>
#define fbo find_by_order
#define ook order_of_key
#define ub upper_bound
#define lb lower_bound
#define pq priority_queue
#define pll pair<ll,ll>
#define error 0.000000000000000001
template<class F>
int no_of_bits(F a){if(sizeof(a)==4)return 32-__builtin_clz(a);return 64-__builtin_clzll(a);}
ll add(ll a,ll b){return ((ll1)a+b)%MOD;}
ll mul(ll a,ll b){return ((ll1)a*b)%MOD;}
ll sub(ll a,ll b){return ((ll1)a-b+MOD)%MOD;}
#define fastio ios::sync_with_stdio(0); cin.tie(0); cout.tie(0)


   long long binpow(long long a, long long b, long long m) {
    a %= m;
    long long res = 1;
    while (b > 0) {
        if (b & 1)
            res = res * a % m;
        a = a * a % m;
        b >>= 1;
    }
    return res;
}


int main(){
	fastio;
	//freopen("input.txt", "r", stdin);
    //freopen("output.txt", "w", stdout);
	//code here
	int n;
	cin>>n;
	while(n--){
	long long a,b;
	cin>>a>>b;
	cout<<binpow(a,b,MOD)<<endl;
}

	return 0;
}
